from  src import mployee_management
from assertpy import assert_that

def test_generate_employee_dictionary():
    employee_dictionary = employee_management.generate_employee_dictionary()
    # assert "email" in employee_dictionary
    # assert "seniority_years" in employee_dictionary
    # assert "female" in employee_dictionary

    assert sorted(["email", "seniority_years", "female"]) == sorted(employee_dictionary.keys())

def test_two_generated_employee_dictionaries_are_different():
    employee_1 = employee_management.generate_employee_dictionary()
    employee_2 = employee_management.generate_employee_dictionary()
    assert assert_that(employee_1).is_not_equal_to(employee_2)

def test_generating_list_of_employees():
    employees = employee_management.generate_list_of_employees(10)
    assert len(employees) == 10

def test_filtering_list_of_employees_for_female():
    input list = {

    }
    female_employees = employee_management.filter_list_of_employees_by_gender(input_list)
    assert len(female_employees) == 0

