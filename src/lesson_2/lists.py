def get_three_highest_numbers_in_the_list(list_of_numbers):
    sorted_list = sorted(list_of_numbers)
    return sorted_list[-3:]




if __name__ == '__main__':
    movies = ["ABC", "DEF", "GHT", "GHT", "OOO"]

    print(len(movies))
    # 1 film
    print(movies[0])
    # last film
    last_movie_index = len(movies) -1
    print(movies[-1])
    #adding a film to the list
    movies.append("FFF")
    movies.insert(2,"ZZZ")
    print(movies)

    print(get_three_highest_numbers_in_the_list([1,2,3,4,5]))


