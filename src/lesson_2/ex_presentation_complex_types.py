#Ex.1
favourite_films = ["ABC", "DEF", "GHT", "GHT", "OOO"]

#Ex.2
def get_three_highest_numbers_in_the_list(list_of_numbers):
    sorted_list = sorted(list_of_numbers)
    return sorted_list[-3:]

#Ex.3
def multiplication_of_numbers_times_3(numbers):
    return [numbers[0] * 3, numbers[1] * 3, numbers[2] * 3]

if __name__ == '__main__':
#Ex.2
    # print(get_three_highest_numbers_in_the_list([1,4,56,7,8,9,77878]))
    # print(get_three_highest_numbers_in_the_list([1,2,3,4,5]))

#Ex.3
    input_numbers = [2, 4, 8]
    output_numbers = multiplication_of_numbers_times_3(input_numbers)
    print(output_numbers)