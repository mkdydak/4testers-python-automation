# shopping_list = ['oranges', 'water', 'chicken', 'potatoes', 'washing liquid']
#
#
# # any item on the list
# print(shopping_list[0])
#
# # last element in the list
# print(shopping_list[-1])
#
# # add element
# shopping_list.append('lemons')
# print(shopping_list)
# print(shopping_list[-1])
#
# # counting the length of the list
# number_of_items_to_buy = len(shopping_list)
# print(number_of_items_to_buy)
#
# # cutting a range from the list 1 - from which element we start, and 2 - the index which stands to the right of the element we want to reach
# first_three_shopping_items = shopping_list[0:3]
# print(first_three_shopping_items)


### DICTIONARIES ###

animal = {
    "name": "Burek",
    "kind": "dog",
    "age": 7,
    "male": True
}


#  download key value ...
dog_age = animal["age"]
print(f"Dog age : {dog_age}")
dog_name = animal["name"]
print(f"Dog name : {dog_name}")

# change of value in the dictionary
animal["age"] = 10
print(animal)

# adding a new value
animal["owner"] = "Staszek"
print(animal)