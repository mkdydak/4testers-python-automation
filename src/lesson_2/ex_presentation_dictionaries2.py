#Ex.1
animals = {
        "name": "Harry",
        "kind": "cat",
        "age": "2",
        "weight": "2.2",
        "male": "True",
        "food": ['jajko', 'banan', 'jablko'],
    }

#Ex.2
def create_person_info(email, phone, city, street):
    return {
        "contact": {
            "email": email,
            "phone": phone
        },
        "address": {
            "city": city,
            "street": street
        }
    }




if __name__ == '__main__':
    person_1 = create_person_info("test@test.pl", +47123456, "Wrocław", "Kukuczki")
    print(person_1)
