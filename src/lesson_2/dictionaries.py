if __name__ == '__main__':
    animals = {
        "name": "Harry",
        "kind": "cat",
        "age": "2",
        "weight": "2.2",
        "male": "True",
        "food": ['jajko', 'banan', 'jablko'],
    }

    print(animals["weight"])
    #new weight
    animals["weight"] = 8.8
    # add new attribute
    animals["like_swimming"] = False
    # remove Key-Value
    del animals["male"]
    print(animals)
    # adding something to the list
    animals["food"].append('snacks')
    print(animals)



