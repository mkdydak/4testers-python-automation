#function definition
def print_area_of_a_circle_with_radius(r):
    area_of_a_circle = 3.1415 * r ** 2
    print(area_of_a_circle)



#call of function
if __name__ == '__main__':
    print_area_of_a_circle_with_radius(5)