#Ex.1
my_friend = {
    "name": "Kasia",
    "age": 36,
    "hobbies": {
        "first_hobbies": "movies",
        "second_hobbies": "books"
    }
}

#Ex.2
emails = ['a@example.com', 'b@example.com']

#Ex.3
def email_address_generator(name, surname):
    name_lower = name.lower()
    surname_lower = surname.lower()
    return (f'{name_lower}.{surname_lower}@4testers.pl')

#Ex.4
def multiplication_of_numbers_times_3(numbers):
    return [numbers[0] * 3, numbers[1] * 3, numbers[2] * 3]

#Ex.5
def students_grade_point_average(grade_list):
    return sum(grade_list) / len(grade_list)


if __name__ == '__main__':
#Ex.1
    #print(my_friend)
#Ex.2
#     print(len(emails))
#     print(emails[0])
#     print(emails[-1])
# #The append() method in Python modifies the list in place, but it does not return the modified list!
#     emails.append('cde@example.com')
#     print(emails)
#Ex.3
    # print(email_address_generator("Janusz", "Nowak"))
    # print(email_address_generator("Barbara", "Kowalska"))
#Ex.4
    # input_numbers = [2, 4, 8]
    # output_numbers = multiplication_of_numbers_times_3(input_numbers)
    # print(output_numbers)
#Ex.5
    print(students_grade_point_average([4.5, 5,3.5,2]))