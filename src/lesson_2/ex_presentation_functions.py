#Ex.1
def square_of_numbers(number):
    return number ** 2

#Ex.2
def volume_of_the_cuboid(a,b,c):
    return a * b * c


#Ex.3
def conversion_Cel_to_Fah(celcius_degrees):
    return (celcius_degrees * 9/5) + 32

#Ex.4
def welcome_message(name, city):
    return (f"Witaj {name}! Miło Cię widzieć w naszym mieście: {city}!")

if __name__ == '__main__':
#Ex.1
    result1 = square_of_numbers(0)
    print(result1)
    result2 = square_of_numbers(16)
    print(result2)
    result3 = square_of_numbers(2.55)
    print(result3)
#Ex.2
    result4 = volume_of_the_cuboid(3,5,7)
    print(result4)
#Ex.3
    result5 = conversion_Cel_to_Fah(20)
    print(result5)
#Ex.4
    result6 = welcome_message("Marta", "Wrocław")
    print(result6)