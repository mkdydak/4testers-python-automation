first_name = "Marta"
last_name = "Dydak"
email = "test@gmail.com"

#print("Mam na imie ", first_name, ". ", "Moje nazwisko to ", last_name, ".", sep="")

# my_bio = "Mam na imie " + first_name + ". Moje nazwisko to " + last_name + "."
#rint(my_bio)

#F-string
#my_bio_using_f_string = f"Mam na imię {first_name}. \nMoje nazwisko to {last_name}. \nMoj email to {email}."
#print(my_bio_using_f_string)

#print(f"Wynik operacji mnożenia 4 przez 5 to {4*5}.")

### Algebra ###
circle_radius = 4
area_of_a_circle_with_radius_5 = 3.14  * circle_radius ** 2
circumference_of_a_circle_with_radius_5 = 2 * 3.14 * circle_radius
print(f"Area of a circle with radius {circle_radius}:", area_of_a_circle_with_radius_5)
print(f"Area of a circum ference with radius {circle_radius}:", circumference_of_a_circle_with_radius_5)

print("--------------------------")

long_mathematical_exprecion = 2 + 3 + 5 * 7
print(long_mathematical_exprecion)