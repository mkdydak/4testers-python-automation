friends_name = "Kasia"
friends_age = 30
friends_pet_amount = 3
#Boolean variables -> HAS
has_driving_license = True
friendship_years = 5.5

print("Name:",friends_name, sep="\n")
print("Age:",friends_age, sep="\n")
print("Pet amount:", friends_pet_amount, sep="\n")
print("Has driving license:", has_driving_license, sep="\n")
print("Friendship years:", friendship_years, sep="\n")

print("-------------------------------")
print("Name:",friends_name,
      "Age:",friends_age,
      "Pet amount:", friends_pet_amount,
      sep="\n"
)

name_surname = "Marta Dydak"
email = "test@test.test"
phone_number = "+48123456789"

#print("Name:",name_surname, "Emial: email", "Phone:", phone_number)

bio = "Name: " + name_surname + "\nEmial: " + email + "\nPhone:" + phone_number

print(bio)

bio_smarter = f"Name: {name_surname} \nEmail: {email} \nPhone: {phone_number}"
print(bio_smarter)


print("-------------------------------")
bio_docstring = f"""Name: {name_surname} 
Email: {email} 
Phone: {phone_number}"""
print(bio_docstring)




