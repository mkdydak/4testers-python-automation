def check_vaccination_needs(kind_of_animal, age_of_animal):
    if kind_of_animal not in ['cat', 'dog']:
        raise ValueError(f"Unsupported amimal type: {kind_of_animal}.")

    if age_of_animal < 1:
        return f"{kind_of_animal.capitalize()} needs a vaccination this year."

    if kind_of_animal == "cat" and age_of_animal % 3 == 0:
        return "Your cat needs a vaccination this year."
    elif kind_of_animal == "dog" and age_of_animal % 2 == 0:
        return "Your dog needs a vaccination this year."

    return f"{kind_of_animal} does not need a vaccination this year."


if __name__ == '__main__':
    print(check_vaccination_needs('cat', 1))
    print(check_vaccination_needs('dog', 2))
    print(check_vaccination_needs('cat', 3))
    print(check_vaccination_needs('dog', 3))
    print(check_vaccination_needs('pig', 1))

