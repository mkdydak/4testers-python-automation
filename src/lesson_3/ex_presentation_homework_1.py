def player_description(player_info):
    return f"The player {player_info['nick']} is of type {player_info['type']} and has {player_info['exp_points']} EXP."


player_info = {
        "nick": 'maestro_54',
        "type": 'warrior',
        "exp_points": 3000
    }


if __name__ == '__main__':
   description = player_description(player_info)
   print(description)
