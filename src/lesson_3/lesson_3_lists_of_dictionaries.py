# animal = {
#     "kind":"dog",
#     "age": 2,
#     "male":"True",
# }
#
# animal2 = {
#     "kind":"cat",
#     "age": 5,
#     "male":"False",
# }
#
# animal_kind = ["dog", "cat", "fish"]

animals = [
{
    "kind":"dog",
    "age": 2,
    "male":"True",
},
{
    "kind":"cat",
    "age": 5,
    "male":"False",
},
{
    "kind":"fish",
    "age": 1,
    "male":"True",
}
]

print(len(animals))
print(animals[-1])
#age of last pet
last_animal = animals[-1]
last_animal_age = last_animal["age"]
print(last_animal_age)
# same - double indexing = double addressing
print(animals[0]["male"])
print(animals[1]["kind"])

#adding another list
animals.append(
{
    "kind":"zebra",
    "age": 3,
    "male": False,
}

)

print(animals)
