#Ex.1
def check_of_the_normal_conditions(temp_degree, hectopascal_degree):
    if temp_degree == 0 and hectopascal_degree == 1013:
        return True
    else:
        return False

#Ex.2
def check_test_result(score):
    if score >= 90:
        return 5
    elif score >= 75:
        return 4
    elif score >= 50:
        return 3
    else:
        return 2

if __name__ == '__main__':
#Ex.1
    # print(check_of_the_normal_conditions(0,1013))
    # print(check_of_the_normal_conditions(1,1013))
    # print(check_of_the_normal_conditions(0, 1014))
    # print(check_of_the_normal_conditions(1, 1014))

#Ex.2
    print(check_test_result(89))