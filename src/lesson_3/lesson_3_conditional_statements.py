#what is a conditional statement?
def print_temperature_description(temperature_in_celsius):
    print(f"Temperature right now is {temperature_in_celsius} Celsius degrees.")
    if temperature_in_celsius > 25:
        print("It's getting hot!")
    else:
        print("It's quite OK.")


if __name__ == '__main__':
    temperature_in_celsius = 21
    print_temperature_description(temperature_in_celsius)



