#dokończyć 
threshold = 0.6

def send_slack_notification(value):
    print(F"Slack message: alert was triggered for value {value} after exceeding the threshold of {threshold}")
def trigger_alert(value):
    print("Alert!")


def check_if_alert_should_be_trigerred(value):
    if value > threshold:
        trigger_alert(value)
        send_slack_notification(value)


if __name__ == '__main__':
    check_if_alert_should_be_trigerred(0.3)
    check_if_alert_should_be_trigerred(0.5)
    check_if_alert_should_be_trigerred(0.9)