import uuid
#Ex.1
def average_of_numbers(numbers):
    return sum(numbers) / len(numbers)

def average_of_two_numbers(number1, number2):
    sum = number1 + number2
    return sum / 2

#Ex.2
def login_data(username):
    email = username + "@example.pl"
    unique_password = str(uuid.uuid4())
    return {
        "email": email,
        "password": unique_password
    }


if __name__ == '__main__':
#Ex.1
    # print(average_of_numbers([3,5,6]))
    # print(average_of_two_numbers(2,5))

    # january = average_of_numbers([-4,1.0, -7,2])
    # print(january)
    # february = average_of_numbers([-13,-9,-3,3])
    # print(february)
    #
    # average_temperature_over_2_months = average_of_two_numbers(january, february)
    # print(average_temperature_over_2_months)

#Ex.2
    print(login_data("marta"))