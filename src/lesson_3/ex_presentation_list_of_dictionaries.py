addresses = [
    {
        "city": 'Wrocław',
        "street": 'Krzycka',
        "house_number": 1,
        "post_code": '57-400'

    },
    {
        "city": 'Wrocław',
        "street": 'Kukuczki',
        "house_number": 23,
        "post_code": '44-100'
    },
    {
        "city": 'Nowa Sól',
        "street": 'Kaszubska',
        "house_number": 13,
        "post_code": '67-100'
    },
]

print(addresses[-1]["post_code"])
print(addresses[1]["city"])
addresses[0]["street"] = "Borowska"
print(addresses)
