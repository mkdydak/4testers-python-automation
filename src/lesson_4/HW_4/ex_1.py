def granting_of_guarantee(year, car_route):
    years = 2024 - year
    if years > 5 or car_route > 60000:
        return False
    else:
        return True

if __name__ == '__main__':
    print(granting_of_guarantee(2020, 30000))
    print(granting_of_guarantee(2020, 70000))
    print(granting_of_guarantee(2016, 30000))
    print(granting_of_guarantee(2016, 120000))
