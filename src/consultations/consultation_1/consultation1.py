


# function/ classes
def foo():
    print("foo")

def bar():
    print("bar")

# Main
if __name__ == '__main__':
    foo()
    bar()

# local variables are in blocks, any variable declared inside a function is local to that function. It can't be accessed outside of it.
# a global variable for the whole file
# it's good for functions to perform a single task
#Object-oriented programming principle - Open close: a class should be open to extension and closed to modification
# SRF -Single Responsibility Functions: a function should perform one task/problem

