from src.consultations.consultation_1 import config
from src.consultations.consultation_1.consultation1 import foo, bar
#consultation as co ---> pandas as pd
# aliases are useful when you have 2 libraries with very similar names
# and it is good to give aliases that differ a lot
def foo_bar():
    print("Start foobar")
    foo()
    #co.foo
    print("Hello from the app!")
    bar()
    print("End foobar")
    # PyCharm help
    # my_name = "Marta"
    # print(f"Name {my_name}")

def login_as_admin():
    print(f"Login {config.admin_email}, Password: {config.admin_password}")


if __name__ == '__main__':
    foo_bar()
    login_as_admin()